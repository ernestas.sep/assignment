import Foundation

//1 assignment (assignment 6 in the extension)
extension Int {
    var roman: String {
        var value = 0
        var string = ""
        let map: [(Int, String)] = [(1000, "M"), (900, "CM"), (500, "D"), (400, "CD"), (100, "C"), (90, "XC"), (50, "L"), (40, "XL"), (10, "X"), (9, "IX"), (5, "V"), (4, "IV"), (1, "I")] //was not able to think about efficient solution without using 4,9,40,90 etc. in map list
        for entry in map
        {
            let quotient = (self - value) / entry.0// how many characters is going to be for given string
            value += quotient * entry.0// value of recorded items
            string += String(repeating: entry.1, count: quotient)//add accordingly amount of value of map table
            if value == self { break }//return if all of the value was recorded
        }
        return string
    }
}

enum ShopError: Error {
    case weekdaysNotFound
}

struct Shop {
    let name:         String
    let adress:       String
    let workSchedule: WorkSchedule

    struct WorkSchedule
    {
        let monday:    WorkHours?
        let tuesday:   WorkHours?
        let wednesday: WorkHours?
        let thursday:  WorkHours?
        let friday: WorkHours?
        let saturday: WorkHours?
        let sunday: WorkHours?
        var weekdaysArray:[WorkHours?] { [monday, tuesday, wednesday, thursday, friday, saturday, sunday] }// using it like this, this would be more simplier and efficient, left "hacky" solution in the commented section below to which dynamically calculates this array from date components, but this is overkill in my opinion.
        
        // hacky way to take properties of the self and then take all date dates compare them in that way sort them
        //        var scheduleDictionary:[(key: String, value: WorkHours?)]?
        //        {
        //            get
        //            {
        //                let mirror = Mirror(reflecting: self)
        //                var workScheduleDict:[String : WorkHours?] = [:]
        //                for child in mirror.children
        //                {
        //                    workScheduleDict[child.label!] = child.value as? WorkHours
        //                }
        //                let formatter = DateFormatter()
        //
        //                var systemWeekdays = formatter.weekdaySymbols
        //                let sunday = systemWeekdays?[0]
        //                if sunday == "Sunday" {
        //                    systemWeekdays!.removeFirst()
        //                    systemWeekdays?.append(sunday!)
        //                }
        //                let sortedScheduleDict = try? workScheduleDict.sorted { (item, item1) -> Bool in
        //                    if let firstIndex = systemWeekdays?.firstIndex(of: item.key.capitalized), let secondIndex = systemWeekdays?.firstIndex(of: item1.key.capitalized)
        //                    {
        //                        return firstIndex < secondIndex
        //                    }
        //                    else { throw ShopError.weekdaysNotFound }
        //                }
        //
        //                return (sortedScheduleDict != nil) ? sortedScheduleDict : nil
        //            }
        //        }
    }

    struct WorkHours: Equatable
    {
        let from: String
        let to: String
        
        init? (from: String, to:String) // failable init to prevent further issues - simple check if time can be converted to date
        {
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "HH:mm"
            if let _ = dateFormatter.date(from: from), let _ = dateFormatter.date(from: to)
            {
                self.from = from
                self.to = to
            }
            else { return nil }
        }

        static func ==(lhs: WorkHours, rhs: WorkHours) -> Bool//Equatable protocol for easier comparison
        {
            return lhs.from == rhs.from && lhs.to == rhs.to
        }
    }
    
    private func centerString(_ stringToCenter: String) -> String // just for logging to make strings equal in length
    {
        if stringToCenter.count > 12 { return stringToCenter }
        var smoothedString:String = stringToCenter
        while smoothedString.count < 12 {
            smoothedString.insert(" ", at: smoothedString.startIndex)
            smoothedString.insert(" ", at: smoothedString.endIndex)
        }
        return smoothedString
    }

    func printSortedList()
    {
        //        guard let scheduleArray = workSchedule.scheduleDictionary else { return } //if using hacky way
        var matchingIntervals:[String] = []
        var times:[String] = []
        
        var startIndex:Int?
        
        for (index, item) in workSchedule.weekdaysArray.enumerated()// if using hacky way - replace with "scheduleDictionary"
        {
            if (startIndex != nil && workSchedule.weekdaysArray[index-1] == item)//use item.value for hacky solution
            {
                matchingIntervals = matchingIntervals.dropLast()
                matchingIntervals.append(centerString("\(startIndex!.roman) - \((index+1).roman)")) // if item in array matches to previuos match and it has previuos index - modify matching days strings array startIndex in this will always have value
            }
            else //of new time - add/change start index, add new time and new day to arrays
            {
                startIndex = index+1
                matchingIntervals.append(centerString("\(startIndex!.roman)"))
                if let workingHours = item//use item.value for hacky solution
                {
                    times.append(centerString("\(workingHours.from) - \(workingHours.to)"))
                }
                else
                { times.append(centerString("CLOSED")) }
            }
        }
        
        print("\(matchingIntervals.joined(separator:"   "))\n\(times.joined(separator:"   "))")
    }
}

//let work: Shop.WorkSchedule = Shop.WorkSchedule(monday: Shop.WorkHours(from: "07:00", to: "21:00"), tuesday: Shop.WorkHours(from: "09:00", to: "21:00"), wednesday: Shop.WorkHours(from: "09:00", to: "21:00"), thursday: Shop.WorkHours(from: "09:00", to: "21:00"), friday:nil, saturday: Shop.WorkHours(from: "09:00", to: "21:00"), sunday: nil)
//
//let shop: Shop = Shop(name: "parduotuve", adress: "adresas", workSchedule: work)
//
//shop.printSortedList()

//2 assigment
extension Array where Element == String// extension for reusability, handy function
{
    func longestCommonPrefix() -> String
    {
        guard var prefix = first.map({ String($0) }) else { return "" }// take first item from array and map it by Character. if it is nil, just return empty String
        for string in dropFirst()// loop to check all items in array (without first)
        {
            if prefix.count == 0 { return "" }// if common prefix becomes 0 - just return empty array, there is no point to continue looping
            while !string.hasPrefix(prefix)
            {
                prefix.removeLast()
            }
        }
        return prefix
    }
}

// 3 assignment
extension String
{
    func lastWordLength() -> Int
    {
        self.reversed().firstIndex(of: " ") ?? self.count
    }
}

//4 assigment
func arrayWithoutDuplicates(_ array:[Int]) -> [Int]
{
    var dictionary: [Int : Any?] = [:] //using dictionary, because it's the fastest way to do so when you can't use the Set (considering the fact that it might be dealing with large arrays)
    for item in array
    {
        dictionary[item] = ""
    }
    let array:[Int] = dictionary.map({ $0.key })

    return array
}


//5 assigment
enum columnNumberError: Error
{
    case unrecognisedCharacter
    case invalidCharacter
}
func getNumberOfColumn(_ column: String) throws -> Int
{
    var count = 0
    for character in [Character](column.uppercased())// cast all characters to uppercase for success
    {
        guard let asciiValue = character.asciiValue else { throw columnNumberError.unrecognisedCharacter }// throw error if stryng has no asciiValue - for example "Ž, Ą..."
        let asciiIntValue = Int(asciiValue)
        if asciiIntValue >= 65 && asciiIntValue <= 91 {//ascIIValues of valid characters is between  65 and 91
            count = count * 26 + (asciiIntValue - 64)
        }
        else { throw columnNumberError.invalidCharacter }// throw if bad Character is entered ex: '
    }
    return count
}

//7 assignment
func smallestTimeDifference(from: String, to: String) -> String?
{
    let dateFormatter = DateFormatter()
    dateFormatter.dateFormat = "HH:mm"

    guard let fromDate = dateFormatter.date(from: from), let toDate = dateFormatter.date(from: to) else { return nil } //check if both of these values can be casted to date
    let calendar = Calendar.current
    let toMin = calendar.component(.hour, from: toDate)*60 + calendar.component(.minute, from: toDate) // cast first date to mins
    let fromMin = calendar.component(.hour, from: fromDate)*60 + calendar.component(.minute, from: fromDate) // cast second date to mins
    let diff = abs(toMin - fromMin) //calculate difference in minutes
    return String(min(diff, 1440-diff)) // take min value of differences 14440 - minutes of the day 60*24
}

//8 assigment
enum profitError: Error {
    case emptyArray
    case negativeNumber
}

func biggestPossibleProfit(_ array:[Double]) throws -> Double
{
    if array.count == 0 { throw profitError.emptyArray }
    var startNumber: Double = -1.0 // means that stock is not purchased

    var returnValue: Double = 0.0
    for (index, number) in array.enumerated()
    {
        if number < 0.0 { throw profitError.negativeNumber }
        if (index != 0 && startNumber == -1.0)
        {
            if number > array[index-1] //check if current iterration number is bigger than previous number, if it is - set position on previous number
            {
                startNumber = array[index-1]
            }
        }
        else
        {
            if (index != 0 && number < array[index-1]) // check if number is current iterration number is smaller - if it is, close position with previous number
            {
                returnValue += array[index-1] - startNumber
                startNumber = -1.0
            }
        }
    }

    if startNumber > -1.0// if position is still open - close it
    {
        returnValue += array.last! - startNumber// if position is open - array wont be empty
    }
    return returnValue
}

//var profit = try? biggestPossibleProfit([7, 4, 5, 4, 2, 8, 7, 4, 5, 4, 2, 8, 7, 4, 5, 4, 2, 8, 7, 4, 5, 4, 2])
//print(profit)
